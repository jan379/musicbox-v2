#!/usr/bin/env bash

# hifiberry mini does not have hardware control, thus we need software:
cp asound.conf /etc/
aplay /usr/share/sounds/alsa/Front_Left.wav

cp wifi_power_safe_off.sh /usr/local/sbin/
chmod 755 /usr/local/sbin/wifi_power_safe_off.sh


