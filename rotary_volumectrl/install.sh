#!/bin/bash

apt install python3-gpiozero

cp volume-knob-daemon.py /usr/local/bin/

chmod 755 /usr/local/bin/volume-knob-daemon.py

cp monitor-volume.service /etc/systemd/system/

systemctl enable monitor-volume.service
systemctl restart monitor-volume.service
