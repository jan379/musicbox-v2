# Supervisor service

This service restarts the nfc-reader services in case it detects
changes to the config file (for example new chipmappings).

It also takes a backup of the old config file in case it was corrupted.


