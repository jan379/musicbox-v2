# install to /usr/local/bin
cp supervisor.sh /usr/local/bin/supervisor.sh
chmod 755 /usr/local/bin/supervisor.sh
# install service file
cp supervisor.service /etc/systemd/system/supervisor.service
# enable service
systemctl enable supervisor.service
# in case that we overwrite an old service file:
systemctl daemon-reload
# start service
systemctl restart supervisor.service
systemctl status supervisor.service
