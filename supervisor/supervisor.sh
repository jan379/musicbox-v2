#!/usr/bin/env bash

# detect changes between two versions of configfile:

configfile="/var/lib/mpd/systemconfig/chipmapping.ini"
backupDir="/var/lib/mpd/configbackups"

test -d ${backupDir} || mkdir ${backupDir}
if test -L ${backupDir}/lastbackup.ini ; then
	echo "Backup already exists";
else
	cp ${configfile} ${backupDir}/initialBackup.ini 
	ln -s ${backupDir}/initialBackup.ini ${backupDir}/lastbackup.ini
fi

checkDifference(){
	checksum1="$(md5sum ${configfile} | cut -d" " -f1)"
	checksum2="$(md5sum ${backupDir}/lastbackup.ini | cut -d" " -f1)"
	if [ "$checksum1" == "$checksum2" ]; then
		echo false 
	else	echo true
	fi
}

while true; do
filedifference="$(checkDifference)"
if [ ${filedifference} == "true" ]; then
	echo "creating backup and restarting nfcdaemon."
	mydate=$(date +%Y-%m-%d-%H-%M-%S)
	cp ${configfile} ${backupDir}/${mydate}.ini
	rm ${backupDir}/lastbackup.ini
	ln -s  ${backupDir}/${mydate}.ini ${backupDir}/lastbackup.ini
	systemctl stop nfcdaemon.service
	systemctl start nfcdaemon.service
fi
sleep 0.5
done

