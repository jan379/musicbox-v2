#!/usr/bin/env bash

# maybe uninstall dependencies*
## apt install python3-pip
## pip3 install mfrc522

echo "Stopping music nfc service."
systemctl stop nfcdaemon.service
echo "Disabling music nfs service."
systemctl disable nfcdaemon.service
echo "Removing serivce file."
rm /etc/systemd/system/nfcdaemon.service
echo "Doing a daemon reload."
systemctl daemon-reload

echo "Remove nfc ddaemon config."
rm -r /var/lib/mpd/systemconfig
echo "Removing nfc daemon binary."
rm /usr/local/bin/nfcdaemon.py
echo ""
