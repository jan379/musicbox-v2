#!/usr/bin/env python3
# -*- coding: utf8 -*-

from mfrc522 import SimpleMFRC522
import signal
import subprocess
import time
import os 
import logging
import configparser
import RPi.GPIO as GPIO

config = configparser.ConfigParser() 
## config.read('/etc/musicbox.cfg')
config.read('/var/lib/mpd/systemconfig/chipmapping.ini')

# get configuration for our logging
loglevel = config.get('main', 'debuglevel')
numeric_loglevel = getattr(logging, loglevel.upper(), None)
if not isinstance(numeric_loglevel, int):
        raise ValueError('Invalid log level: %s' % loglevel)
logging.basicConfig(filename='/var/log/musicbox.log', level=numeric_loglevel)

logging.info("Initializing musicbox!")
logging.info("More info can be found at https://blog.blabla.berlin")

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    logging.info("Ctrl+C captured, ending read.")
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

def mapCardToPlaylist(uid):
    try:
        # lookup if uid exists in config file
        playlist = config.get('playlists', uid)
        return playlist
    except:
        logging.info("NFC chip is not mapped in config: " + uid)
        config['playlists'][uid] = 'notYetMapped'
        with open('/var/lib/mpd/systemconfig/chipmapping.ini', 'w') as configfile:
            config.write(configfile)
            configfile.close()
        return "noCard"


# This loop keeps checking for chips. If one is near it will get the UID and change to the according playlist. 
def main():
    # Create an object of the class MFRC522
    reader = SimpleMFRC522()
    main.uid = "initUid"
    main.uidPrev = "initUidPrev"
    main.mpdStatus = "initState"
    main.errorcount = 0
    # Welcome message
    logging.info('Welcome to musicbox daemon!')
    logging.info('use "systemctl start/stop musicbox" to control this service. ')
    while continue_reading:

        time.sleep(0.4)
    
        # Get UID of the card
        uid = reader.read_id_no_block()
        if uid == None:
            main.errorcount += 1
            if main.errorcount > 2:
                main.uid = "noCard"
        else:
            main.uid = str(uid)
            main.errorcount = 0
        mappedPlaylist = str(mapCardToPlaylist(main.uid))
        if main.uid != "noCard":
            if main.uid == main.uidPrev:
                # do nothing
                logging.debug('Nothing changed: Card '+main.uidPrev +' is in place.')
                logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "playing":
                    subprocess.call(["/usr/bin/mpc", "play"])
                    logging.info('Start playing ' + mappedPlaylist)
                    main.mpdStatus = "playing"

            else:
                logging.info('Card has changed: Card ' +main.uidPrev +' has been replaced by '+main.uid+'.')
                # start playing something, defined by the card
                if main.mpdStatus != "cleared":
                    subprocess.call(["/usr/bin/mpc", "clear"])
                    main.mpdStatus = "cleared"
                    logging.info('Clear playlist.')
                    logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "loaded":
                    subprocess.call(["/usr/bin/mpc", "load", mappedPlaylist])
                    main.mpdStatus = "loaded"
                    logging.info('Load playlist.')
                    logging.debug('MPD status: ' + main.mpdStatus )
                if main.mpdStatus != "playing":
                    subprocess.call(["/usr/bin/mpc", "play"])
                    logging.info('Start playing ' + mappedPlaylist)
                    main.mpdStatus = "playing"
                    logging.debug('MPD status: ' + main.mpdStatus )

            main.uidPrev = main.uid 
            
        else:
            logging.debug('No Card can be read...')
            logging.debug('MPD status: ' + main.mpdStatus )
            # stop playing anything
            if main.mpdStatus != "paused":
                subprocess.call(["/usr/bin/mpc", "pause"])
                logging.info('Paused music player daemon...')
                main.mpdStatus = "paused"
    
main()

