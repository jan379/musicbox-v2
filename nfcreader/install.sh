# install dependencies
apt install python3-pip
pip3 install mfrc522

# install to /usr/local/bin
cp nfcdaemon.py /usr/local/bin/nfcdaemon.py
chmod 755 /usr/local/bin/nfcdaemon.py
# ensure config exists
mkdir /var/lib/mpd/systemconfig
if [ -f  /var/lib/mpd/systemconfig/chipmapping.ini ]; then
	echo "Found config, not creating a new one"
else
	echo "No config found, creating a new one"
	cp nfcdaemon.cfg_example /var/lib/mpd/systemconfig/chipmapping.ini
fi
chown mpd: /var/lib/mpd/systemconfig
# install service file
cp nfcdaemon.service /etc/systemd/system/nfcdaemon.service
# enable service
systemctl enable nfcdaemon.service
# in case that we overwrite an old service file:
systemctl daemon-reload
# start service
systemctl restart nfcdaemon.service
systemctl status nfcdaemon.service
