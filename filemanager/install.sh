# get and install filebrowser to /usr/local/bin
curl -fsSL https://filebrowser.xyz/get.sh | bash 

# install service file
cp filebrowser.service /etc/systemd/system/filebrowser.service
# enable service
systemctl enable filebrowser.service
# in case that we overwrite an old service file:
systemctl daemon-reload
# start service
systemctl restart filebrowser.service
